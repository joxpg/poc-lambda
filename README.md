#### Prueba 


El presente repositorio está estructurado de la siguiente forma:
- app: Contiene el código de la función lambda
- terraform: Contiene la estructura necesaria para crear en AWS los componentes necesarios para que el servicio funcione.


La arqutectura propuesta para el servicio y el despliegue del mismo está representada a alto nivel en el siguiente diagrama: 

	
![name](/images/shipify.png?raw=true)


Se usa Gitlab para la integración y despliegue continuo, además Terraform para la creación de la infraestructura en AWS requerida para desplegar el servicio.
